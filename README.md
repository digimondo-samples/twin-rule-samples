# Digitaltwin Rules Examples

Rules are defined to execute certain actions based on state-values of digital twins.

Whether those rules evaluate to `true` or `false`, is specified with a set of conditions. Those conditions can be created with a UI-picker, or custom javascript-code.

In the following are some examples how a javascript-rule-condition can look like.

[[_TOC_]]


## Time-based

Parameters/State-Metrics: `temperature`

Only evaluate the rule to `true` if the `temperature`-state-value is at least 20 and the local time is between 21h and 07:59h.

```javascript
const currentDate = new Date(new Date().toLocaleString('en-US', {timeZone: 'Europe/Berlin'}));
return temperature >= 20 && (currentDate.getHours() >= 21 || currentDate.getHours() <= 7);
```

Or with even a little finer grain:

```javascript
const currentDate = new Date(new Date().toLocaleString('en-US', {timeZone: 'Europe/Berlin'}));
// "now" is between 14:30 and 15:59
const firstPeriod = (currentDate.getHours() == 14 && currentDate.getMinutes() >= 30) || (currentDate.getHours() > 14 && currentDate.getHours() < 16);
// ...or between 22:45 and 05:59 at night
const secondPeriod = (currentDate.getHours() == 22 && currentDate.getMinutes() >= 45) || (currentDate.getHours() > 22 || currentDate.getHours() <= 5);
return temperature >= 20 && (firstPeriod || secondPeriod);
```


## Compare two states

Parameters/State-Metrics: `temperature_inside`, `temperature_outside`, and `humidity`

Only evaluate the rule to `true` if the `temperature_inside`-state-value is bigger than `temperature_outside` and the `humidity` is at least 60 percent.

```javascript
return temperature_inside > temperature_outside && humidity >= 60;
```

## Threshold-Timeseries

Parameters/State-Metrics: `temperature_24h_avg`, `temperature`

The `temperature_24_avg` metric is setup to average all `temperature` values of the last 24 hours, while `temperature` is just set to `LAST`.

Evaluate this rule to `true` if the temperature rises 5 degrees over the 24h average.

```javascript
return temperature_24h_avg + 5 < temperature;
```
